import React from 'react';
import ReactDOM from 'react-dom';
import './App.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import history from "./services/history";
import { Router } from "react-router-dom";

ReactDOM.render(
<React.StrictMode>
  <Router  history={history}>
    <App/>
  </Router>
</React.StrictMode>,
document.getElementById('root')
);

serviceWorker.unregister();
