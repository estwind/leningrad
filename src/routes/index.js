import React from 'react';
import {Route, Switch} from "react-router-dom";
import Contact from "../pages/Contact";
import About from "../pages/About";
import Service from "../pages/Service";
import Product from "../pages/Product";

export default function Routes() {
  return (
  <Switch>
    <Route exact path="/" activeClassName="active" component={About}/>
    <Route path="/service" component={Service}/>
    <Route path="/product" component={Product}/>
    <Route path="/contact" component={Contact}/>
  </Switch>
  )
}
