import React from 'react';
import { NavLink } from "react-router-dom";

class Header extends React.Component {
  constructor(props) {
    super(props);    this.state = {
      isExpanded: false
    };
  }
  
  handleToggle(e) {
    e.preventDefault();
    this.setState({
      isExpanded: !this.state.isExpanded
    });
  }
  
  render() {
    const {isExpanded} = this.state;
    
    return (
      <header className='header'>
        <div className='container top'>
          <div className='top_logo'>
            ЛОГО
          </div>
        </div>
        <nav className='nav'>
          <div className="hamburger" aria-hidden="true" onClick={e => this.handleToggle(e)}>
            <span className='hamburger_line'> </span>
          </div>
          <ul className={`nav_list container collapsed ${isExpanded ? "is-expanded" : ""}` }>
                <li className='nav_item'><NavLink className='nav_link' to='/' exact>О компании</NavLink></li>
                <li className='nav_item'><NavLink className='nav_link' to='/service'>Услуги</NavLink></li>
                <li className='nav_item'><NavLink className='nav_link' to='/product'>О продукте</NavLink></li>
                <li className='nav_item'><NavLink className='nav_link' to='/contact'>Контакты</NavLink></li>
          </ul>
        </nav>
      </header>
    );
  }
}

export default Header;