import React from 'react';

export default function Footer() {
  return (
    <footer className='footer'>
        <a className='footer_link' href="tel:+79999999999">+7 (999) 999 99 99</a>
    </footer>
  )
}