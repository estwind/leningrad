import React from 'react';

export default function About() {
  return (
  <div className='container about'>
    <h1 className='about_head'>О КОМПАНИИ</h1>
    <p className='about_text'>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod <br/>tempor
      incididunt ut labore et <span className='bolder'>dolore magna aliqua</span>.
    </p>
    <p className='about_text'>
      Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut <br/>aliquip ex ea
      commodo consequat.
    </p>
    <p className='about_subtext'>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit!
    </p>
    <button className='about_button'>Купить продукт</button>
    <section className='review'>
      <h2 className='review_head'>Отзывы наших покупателей</h2>
      <div className='review_container'>
        <div className='review_item'>
          <div className='review_img'></div>
          <div className='review_text-wrap'>
            <div className='review_name'>Roger Stevenson</div>
            <p className='review_text'>American skeptic producer and author Brian Dunning believes Polybius to be an
              urban legend that grew out of exaggerated and distorted tales of an early release version of Tempest that
              caused problems with photosensitive epilepsy, motion sickness, and vertigo.</p>
          </div>
        </div>
        <div className='review_item'>
          <div className='review_img'></div>
          <div className='review_text-wrap'>
            <div className='review_name'>Roger Stevenson</div>
            <p className='review_text'>American skeptic producer and author Brian Dunning believes Polybius to be an
              urban legend that grew out of exaggerated and distorted tales of an early release version of Tempest that
              caused problems with photosensitive epilepsy, motion sickness, and vertigo.</p>
          </div>
        </div>
      </div>
    
    </section>
    <section className='office-hours'>
      <h2 className='office-hours_head'>Часы работы</h2>
      <table className='office-hours_table'>
        <tbody>
          <tr>
            <td>ПН-ПТ</td>
            <td>10-18</td>
          </tr>
          <tr>
            <td>СБ</td>
            <td>10-16</td>
          </tr>
          <tr>
            <td>ВС</td>
            <td>12-16</td>
          </tr>
        </tbody>
      </table>
    
    </section>
  </div>
  )
  
}