import React from 'react';
import Header from './static/Header'
import Footer from './static/Footer'
import Routes from './routes';

function App() {
  return (
  <div className='wrapper'>
    <Header/>
    <main className='content'>
        <Routes />
    </main>
    <Footer/>
  </div>
)}

export default App;
